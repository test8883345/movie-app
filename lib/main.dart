import 'routers/app_routers.dart';
import 'package:flutter/material.dart';
import 'package:material_color_generator/material_color_generator.dart';

import 'core/color.dart';
import 'mobx/movie.dart';

final MovieMobx movieMobx = MovieMobx();

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: generateMaterialColor(color: blueBr),
      ),
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
