import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:gap/gap.dart';
import 'package:movie_app/core/color.dart';

import '../main.dart';
import '../routers/app_routers.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _controllerSearch = TextEditingController();

  @override
  void initState() {
    movieMobx.initial();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies Collection'),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton.small(
        onPressed: () {
          context.pushRoute(NewMovieRoute()).then((value) {
            movieMobx.filterMovie('');
            _controllerSearch.clear();
          });
        },
        child: const Icon(Icons.add),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  TextFormField(
                    controller: _controllerSearch,
                    cursorColor: blueBr,
                    autocorrect: false,
                    onChanged: (value) {
                      movieMobx.filterMovie(value);
                    },
                    maxLength: 100,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    obscureText: false,
                    decoration: InputDecoration(
                      hintText: 'Search Movie',
                      counterText: '',
                      isDense: true,
                      contentPadding: const EdgeInsets.fromLTRB(12, 20, 0, 9),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                          color: blueBr,
                        ),
                      ),
                      fillColor: blueBr.withOpacity(0.1),
                      filled: true,
                    ),
                  ),
                ],
              ),
            ),
            Observer(
                builder: (context) => Container(
                      width: MediaQuery.of(context).size.width,
                      child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: movieMobx.moviesFiltered.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () => context
                                  .pushRoute(UpdateMovieRoute(id: movieMobx.moviesFiltered[index].id))
                                  .then((value) {
                                movieMobx.filterMovie('');
                                _controllerSearch.clear();
                              }),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          movieMobx.moviesFiltered[index].title.toString(),
                                          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                                        ),
                                        Row(
                                          children: [
                                            const Text(
                                              'Directed by ',
                                              style: TextStyle(fontSize: 14),
                                            ),
                                            Text(
                                              movieMobx.moviesFiltered[index].director.toString(),
                                              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                        const Gap(12),
                                        const Text(
                                          'Genre:',
                                          style: TextStyle(fontSize: 12),
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              height: 20,
                                              width: MediaQuery.of(context).size.width - 40,
                                              child: ListView.builder(
                                                  scrollDirection: Axis.horizontal,
                                                  itemCount: movieMobx.moviesFiltered[index].genres.length,
                                                  itemBuilder: (context, indexGenre) {
                                                    return Row(
                                                      children: [
                                                        Text(
                                                          movieMobx.moviesFiltered[index].genres[indexGenre].name
                                                              .toString(),
                                                          style: const TextStyle(fontSize: 14),
                                                        ),
                                                        const Gap(8),
                                                      ],
                                                    );
                                                  }),
                                            ),
                                          ],
                                        ),
                                        const Gap(12),
                                        const Text(
                                          'Summary:',
                                          style: TextStyle(fontSize: 12),
                                        ),
                                        Text(
                                          movieMobx.moviesFiltered[index].summary.toString(),
                                          style: const TextStyle(fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const Gap(14),
                                  Container(
                                    height: 4,
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.grey[200],
                                  ),
                                ],
                              ),
                            );
                          }),
                    )),
            const Gap(40),
          ],
        ),
      ),
    );
  }
}
